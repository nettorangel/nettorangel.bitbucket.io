# Método do Trapézio Composto

# Recebe o limite inferior, limite superior e o número de  partições. 
# Retorna o valor da integral aplicando a regra do trapezio composto.
function integral(a, b, n)
  h = (b-a)/n # dimensão de cada partição
  sum = 0

  f(x) = x * x # função genérica

  for i=1 : n-1
    sum = sum + f(a) + f(a+h)
    a = a + h
  end

  return sum * h / 2
end

println("Entre com o número de trapezios d  esejados: ")
n = parse(Int, readline())
@time println("Resultado: ", integral(0, 10, n))

# site para verificar os resultados do calculo: https://www.integral-calculator.com/