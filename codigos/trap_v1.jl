using Distributed

@everywhere f(x) = 10*x - x^2

function integral(id, a, b, n)
  h = (b-a)/n 
  sum = 0

  for i=1 : n-1
    func_a = @spawnat id f(a)
    func_ah = @spawnat id f(a+h)
    sum +=  fetch(func_a) + fetch(func_ah) 
    a += h
  end

  return sum * h / 2
end

id = myid()

println("Entre com o número de trapezios d  esejados: ")
n = parse(Int, readline())
@time answer = integral(id, 0, 10, n)

println("Resultado: ", answer)
