using Distributed 

@everywhere function trap(id, a, b, n)
  h = (b-a)/n 
  sum = 0
  
  f(x) = x*x

  @sync @distributed for i=1 : n-1
    #func_a = @spawnat id f(a)
    #func_ah = @spawnat id f(a+h)
    #sum +=  fetch(func_a) + fetch(func_ah) 
    sum += f(a) + f(a+h)
    a += h
  end

  return sum * h / 2
end

function task()
  
  a = 0
  b = 3
  println("Entre com o número de trapezios desejados: ")
  n = parse(Int, readline())

  id = myid()

  np = length(procs())
  local_n = n/np
  sum = 0

  for p in 1:np
    res = @spawnat p trap(myid(), a, b, local_n)
    sum += fetch(res)
  end 

  if id == 1
    integral = sum / np
    println("Com n = $n trapezios, nossa estimativa da integral de $a a $b é $integral")
  end
end

@time task()