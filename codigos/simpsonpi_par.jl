using Distributed

@everywhere function simpson(id, a, b, n)  
  f(x) = sqrt(1 - x^2)

  h = (b-a)/n
  y0 = f(a + 0*h)
  yn = f(a + n*h)

  sum1 = 0
  sum2 = 0

  for i in 1:n-1
    if i%2 == 0
      sum1 = sum1 + f(a + i*h)
    else
      sum2 = sum2 + f(a + i*h)
    end
  end

  sum = (h/3) * (y0 + yn + 2*sum1 + 4*sum2)

  return sum
end

function main()
  id = myid()

  a = 0
  b = 1
  println("Quantidade de amostra: ")
  n = parse(Int, readline())

  np = length(procs())
  local_n = n/np
  sum = 0

  for p in 1:np
    Pi = @spawnat p simpson(myid(), a, b, local_n)
    sum += fetch(Pi)
  end 

  if id == 1
    Pi = 4 * sum / np
    println("Estimativa de Pi: $Pi")
    println("  erro : $(abs(Pi- π))")
  end
end

@time main()