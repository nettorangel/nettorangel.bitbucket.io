function f(x)
  return sqrt(1 - x^2)
end

function simpson(a, b, n) 
  h = (b-a)/n
  y0 = f(a + 0*h)
  yn = f(a + n*h)

  sum1 = 0
  sum2 = 0

  for i=1 : n-1
    if i%2 == 0
      sum1 = sum1 + f(a + i*h)
    else
      sum2 = sum2 + f(a + i*h)
    end
  end
    
  sum = (h/3) * (y0 + yn + 2*sum1 + 4*sum2)

  return sum
end

Pi = @time 4 * simpson(0, 1, 100000)
println("Valor: ", Pi)
