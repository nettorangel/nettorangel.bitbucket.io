using Distributed
addprocs(8)

using Base.Libc, Printf, Random

@everywhere using Random, Printf

@everywhere function cnt_pi(i, n)
  Random.seed!(time_ns()+i)

  j = 0
  cnt = 0
  for j = j:n
      x = (rand(Float64))
      y = (rand(Float64))
      z = x*x + y*y
      if z < 1.0
          cnt = cnt+1
      end
  end

  print("Core $i identificou $cnt amostras dentro da circunferência\n")
  return cnt
end

function main()
  n = 0
  id = myid()
  if id == 1
    print("Número de amostras desejadas...\n")
    n = readline()

  end

  np = length(procs())
  fraction = n/np
  sum = 0.0
  i = 2
 
  for i=i:np
    est4pi = @spawnat i cnt_pi(myid(),fraction)
    sum = sum + fetch(est4pi)
  end 

  if id == 1
    est4pi = 4.0*sum/n
    sleep(0.2)
    @printf("Estimativa de Pi:  %.15lf",est4pi)
    @printf("  erro : %.3e\n",abs(est4pi- π))
  end 

end

@time main()