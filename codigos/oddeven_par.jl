using Random, Distributed

function generate_list(n) 
  Random.seed!(42)
  a = []

  @sync @distributed for i in 1:n
    x = mod(rand(Int), 100)
    append!(a, x)
  end

  return a
end

function print_list(a, title)
  println(title)
  println(a)
end

function read_list(n)
  a = []
  println("Entre com os elementos da lista")
  @sync @distributed for i = 1:n
    n = parse(Int64, readline())
    push!(a, n)
  end
  return a
end

function swap(list, i, j)
  temp = list[i]
  list[i] = list[j]
  list[j] = temp
end

function sort(a, idx, n)
  i = idx
  while i < n
     if (a[i] > a[i+1])
        swap(a, i, i+1)
     end
     i += 2
  end
end

function odd_even_sort(a)
  n = length(a)
  @sync @distributed for phase in 1:n
    if mod(phase, 2) == 0 # Even phase
      sort(a, 1, n)
    else # Odd phase 
      sort(a, 2, n)
    end
  end
end

id = myid()

if id == 1
  print("Lista gerada randomicamente ou digitada: ['g' ou 'f'] ")
  opcao = readline()
  print("Qual o tamanho da lista: ")
  n = parse(Int, readline())

  if (opcao == "g")
    a = generate_list(n)
    #print_list(a, "Antes da ordenação")
  else
    #a = [5, 4, 2, 1, 8, 4, 3, 7, 9, 2]
    a = read_list(n)
  end
end

@time odd_even_sort(a)

#print_list(a, "Depois da ordenação")