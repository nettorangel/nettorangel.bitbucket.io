using Random 

function generate_list(n) 
  Random.seed!(42)
  a = []
  for i in 1:n
    x = mod(rand(Int), 100)
    append!(a, x)
  end
  return a
end

function print_list(a, title)
  println(title)
  println(a)
end

function read_list(n)
  a = []
  println("Entre com os elementos da lista")
  for i = 1:n
    n = parse(Int16, readline())
    push!(a, n)
  end
  return a
end

function odd_even_sort(a)
  n = length(a)
  for phase in 1:n
    if mod(phase, 2) == 0 # Even phase
        i = 2
        while i < n
           if (a[i] > a[i+1])
              temp = a[i]
              a[i] = a[i+1]
              a[i+1] = temp
           end
           i = i + 2
        end
    else # Odd phase 
        i = 1
        while i < n
          if (a[i] > a[i+1])
              temp = a[i]
              a[i] = a[i+1]
              a[i+1] = temp
          end
          i = i + 2
        end
    end
  end
end

print("Qual o tamanho da lista: ")
n = parse(Int32, readline())

print("Lista gerada randomicamente ou digitada: ['g' ou 'f'] ")
opcao = readline()

if (opcao == "g")
  a = generate_list(n)
  #print_list(a, "Antes da ordenação")
else
  #a = [5, 4, 2, 1, 8, 4, 3, 7, 9, 2]
  a = read_list(n)
end

@time odd_even_sort(a)
#print_list(a, "Depois da ordenação")