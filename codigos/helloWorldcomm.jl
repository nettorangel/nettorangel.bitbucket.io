import MPI
function main()
   MPI.Init()
   comm = MPI.COMM_WORLD
   size = MPI.Comm_size(comm)
   rank = MPI.Comm_rank(comm)
   message = zeros(100,1)

   if (rank == 0)
      message = "Hello World"
      for i = 1:size-1
         MPI.Send(message, 0, 0, comm)
      end
  else 
      MPI.Recv!(message, i, 0, comm)
   end
   print( "Processo = $(rank) : $(message)");
   MPI.Barrier(comm)
   MPI.Finalize()
end
main()