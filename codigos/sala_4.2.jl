using Printf, Random

function cnt_pi(n)
  Random.seed!(time_ns())

  j = 0
  cnt = 0

  for j = j:n-1
      x = (rand(Float64))
      y = (rand(Float64))
      z = x*x + y*y
      if z < 1.0
          cnt += 1
      end
  end

  return cnt
end

function main()
  n = 0
  est4pi = 0.0
  cnt = 0

  print("N�mero de amostras desejadas...\n")
  n = readline()

  for i = 1:n 
    cnt = cnt + cnt_pi(Int(n)) 
  end
  
  est4pi = 4.0*cnt/n
  @printf("Estimativa final de Pi: %.15lf",est4pi)
  @printf("  erro : %.3e\n",abs(est4pi- ?))
end

@time main()