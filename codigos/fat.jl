import MPI

function fat(n)

  if (n == 0)
     return 1;
   end
  return n*fat(n-1);
end

function main()

   n = 4;
   
   MPI.Init()
   comm = MPI.COMM_WORLD
   size = MPI.Comm_size(comm)
   rank = MPI.Comm_rank(comm)

   f = fat(n);
   
   print("Fatorial de $(n) = $(f)");

   MPI.Barrier(comm)
   MPI.Finalize()
end
main()