import MPI
function fat(n, sz)
   if(n>0)
      return n*fat(n-sz,sz)
   else
      return 1
   end
end

function main()
   n = 4;

   MPI.Init()
   comm = MPI.COMM_WORLD
   size = MPI.Comm_size(comm)
   rank = MPI.Comm_rank(comm)

   if (rank == 0)
      parcial = fat(n-rank,size)
      for q = 1:size-1
         MPI.Recv!(resul, i, 0, comm)
         parcial = parcial * resul
      end
      print("O resultado do fatorial de $(n) é = $(parcial)")
   else
      resul = fat(n-rank,size)
      MPI.Send(resul, 0, 0, comm)
   end
   
   MPI.Barrier(comm)
   MPI.Finalize()
end
main()