using Distributed
addprocs(8)

using Base.Libc, Printf, Random
@everywhere using Printf, Random

@everywhere function estimate_pi(i, n)
  Random.seed!(time_ns()*i)

  j = 0
  cnt = 0
  for j = j:n-1
      x = (rand(Float64))
      y = (rand(Float64))
      z = x*x + y*y
      if z < 1.0
          cnt = cnt+1
      end
  end

  estimate = cnt*4.0/n
  @printf("Core %d estima Pi em : %.15f",i,estimate)
  @printf("  erro : %.3e\n",abs(estimate - π))

  return estimate
end

function main()
  n = 0
  id = myid()
  np = length(procs())


  if id == 1
    print("Número de amostras desejadas...\n")
    n = readline()

  end

  i = 2
  sum = 0.0

  for i=i:np
    est4pi = @spawnat i estimate_pi(myid(),n)
    sum = sum + fetch(est4pi)
  end 

  if id == 1
    est4pi = sum/np
    @printf("Estimativa final de Pi: %.15lf",est4pi)
    @printf("  erro : %.3e\n",abs(est4pi- π))
  end 

end

@time main()