using Printf, Random

function estimate_pi(n)
  Random.seed!(time_ns())

  j = 0
  cnt = 0

  for j = j:n-1
      x = (rand(Float64))
      y = (rand(Float64))
      z = x*x + y*y
      if z < 1.0
          cnt += 1
      end
  end

  estimate = cnt*4.0/n
  return estimate
end

function main()
  n = 0
  est4pi = 0.0
  sum = 0.0
  print("N�mero de amostras desejadas...\n")
  n = readline()

  println(n)

  for i = 1:n 
    sum = sum + estimate_pi(Int(n))
  end

  est4pi = sum/n
  @printf("Estimativa final de Pi: %.15lf",est4pi)
  @printf("  erro : %.3e\n",abs(est4pi- ?))
end


@time main()