import MPI


function fat(n)
  if(n>0)
    return n*fat(n-1)
  else
    return 1
   end
end

function main()
   n = 4;

   MPI.Init()
   comm = MPI.COMM_WORLD
   size = MPI.Comm_size(comm)
   rank = MPI.Comm_rank(comm)

   if (rank == 0) 
      parcial = fat(n)
      for q = 1:size-1
         MPI.Recv!(resul, q, 0, comm)
         parcial = parcial * resul
      end
      print("O resultado do fatorial de $(n) é = $(parcial)");
   end
   if(rank == 1)
      resul = fat(n-1);
      MPI.Send(resul, 0, 0, comm)
   end
   if(rank == 2)
      resul = fat(n-2);
      MPI.Send(resul, 0, 0, comm)
   end
   if(rank == 3)
      resul = fat(n-3);
      MPI.Send(resul, 0, 0, comm)
   end

   MPI.Barrier(comm)
   MPI.Finalize()
end
main()